/**
 * Copyright 2015, Yahoo! Inc.
 * Copyrights licensed under the New BSD License. See the accompanying LICENSE file for terms.
 */
/**
 * Forked from https://github.com/huanz/webpack-strip-block
 */
/*jslint node:true */
"use strict";

var loaderUtils = require('loader-utils');

function regexEscape(str) {
    return str.replace(/([\^|\$|\.|\*|\+|\?|\=|\!|\:|\\|\/|\(|\)|\[|\]|\{|\}])/gi, '\\$1');
}

function StripBlockLoader(source) {
    this.cacheable && this.cacheable();
    //this.value = source;
    var query = loaderUtils.parseQuery(this.query);

    if (query && query.blocks) {
        var start = regexEscape(query.start || '/*');
        var end = regexEscape(query.end || '/*');
        query.blocks.forEach(function (block) {
            var regex = new RegExp('[\\t ]*' + start + ' ?(' + block + '):start ?' + end + '[\\s\\S]*?' + start + ' ?\\1:end ?' + end + '[\\t ]*\\n?', 'g');
            source = source.replace(regex, '');
        });
    }

    /*if (this.callback) {
        return this.callback(null, source);
    }*/
    return source;
}
module.exports = StripBlockLoader;

/*module.exports.loader = function () {
    return __filename + '?' + [].slice.call(arguments, 0).map(function (fn) {
        return 'start=' + fn;
    }).join(',');
};*/
